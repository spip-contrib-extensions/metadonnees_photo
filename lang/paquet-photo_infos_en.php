<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-photo_infos?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'photo_infos_description' => 'This plugin allows to view EXIF, IPTC and GPS data from a JPEG file.

EXIF informations are handled without using the PHP’s EXIF extension, but through a script by Vinay Yadav (under LGPL license).

IPTC informations requires the "iptcparse" PHP function and use the class "class_iptc" by Alex Arica.',
	'photo_infos_nom' => 'Photo metadata',
	'photo_infos_slogan' => 'Display EXIF, IPTC and GPS data from a JPEG file'
);
