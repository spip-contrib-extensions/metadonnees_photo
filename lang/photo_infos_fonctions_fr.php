<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/metadonnees_photo.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Auteur',

	// C
	'categorie' => 'Catégorie',
	'copyright' => 'Copyright',
	'credits' => 'Crédits',

	// D
	'datecreation' => 'Date de création',
	'distance' => 'Distance',

	// F
	'flash' => 'Flash',
	'focale' => 'Focale',
	'focale35mm' => 'Focale éq. 35mm',

	// I
	'intitule' => 'Intitulé',

	// L
	'latitude' => 'Latitude',
	'legende' => 'Légende',
	'longitude' => 'Longitude',

	// M
	'motscles' => 'Mots-clés',

	// O
	'objectif' => 'Objectif',
	'ouverture' => 'Ouverture',
	'ouverturemax' => 'Ouverture max.',

	// P
	'pays' => 'Pays',
	'provinceetat' => 'Province, État',

	// S
	'sensibilite' => 'Sensibilité',

	// T
	'titre' => 'Titre',

	// V
	'ville' => 'Ville',
	'vitesse' => 'Vitesse'
);
