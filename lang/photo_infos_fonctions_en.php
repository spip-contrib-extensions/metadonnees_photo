<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/photo_infos_fonctions?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Author',

	// C
	'categorie' => 'Category',
	'copyright' => 'Copyright',
	'credits' => 'Credits',

	// D
	'datecreation' => 'Creation date',
	'distance' => 'Distance',

	// F
	'flash' => 'Flash',
	'focale' => 'Focal length',
	'focale35mm' => 'Focal eq. 35mm',

	// I
	'intitule' => 'Headline',

	// L
	'latitude' => 'Latitude',
	'legende' => 'Legend',
	'longitude' => 'Longitude',

	// M
	'motscles' => 'Keywords',

	// O
	'objectif' => 'Lens',
	'ouverture' => 'Aperture',
	'ouverturemax' => 'Max. aperture',

	// P
	'pays' => 'Country',
	'provinceetat' => 'Province, State',

	// S
	'sensibilite' => 'Iso equiv.',

	// T
	'titre' => 'Title',

	// V
	'ville' => 'City',
	'vitesse' => 'Speed'
);
